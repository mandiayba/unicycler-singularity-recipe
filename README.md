> Attention ! projet encore en dev

# Unicycler singularity recipe

Ce projet contient une recette singularity pour créer des images singularity pour l'outil [Unicycler](https://github.com/rrwick/Unicycler)


## Accéder à une image singularity de Unicycler

Une image singularity de Unicycler version 0.4.8 est proposée via Forgemia.
La commande `singularity pull ...` suivante permet de télécharger l'image (on peut cloner le dossier du projet et y mettre l'image pour permettre ensuite de tester l'exécution avec les données exemples).

```
git clone https://forgemia.inra.fr/mandiayba/unicycler-singularity-recipe.git

singularity pull unicycler-0.4.8.sif oras:registry.forgemia.inra.fr/mandiayba/unicycler-singularity-recipe/unicycler:0.4.8

ls unicycler-0.4.8.sif
```

## Exécuter l'image singularity
L'image téléchargée précédemment est un exécutable que vous pouvez lancer comme suit (les données et les commandes proviennent de la [page test fournie avec Unicycler](https://github.com/rrwick/Unicycler/tree/master/sample_data)) :

```
cd unicycler-singularity-recipe

./unicycler-0.4.8.sif --help

./unicycler-0.4.8.sif --help_all

./unicycler-0.4.8.sif -1 sample_data/short_reads_1.fastq.gz -2 sample_data/short_reads_2.fastq.gz -o sample_data/output_dir

./unicycler-0.4.8.sif -l sample_data/long_reads_high_depth.fastq.gz -o sample_data/output_dir

./unicycler-0.4.8.sif -1 sample_data/short_reads_1.fastq.gz -2 sample_data/short_reads_2.fastq.gz -l sample_data/long_reads_low_depth.fastq.gz -o sample_data/output_dir

./unicycler-0.4.8.sif -1 sample_data/short_reads_1.fastq.gz -2 sample_data/short_reads_2.fastq.gz -l sample_data/long_reads_high_depth.fastq.gz -o sample_data/output_dir

```

## Re-construire l'image singularity de Unicycler en local

Les commandes suivantes permettent de re-construire l'image singularity de Unicycler 0.4.8  à partir des recettes fournies dans le projet.
 
 ```
 cd unicycler-singularity-recipe/
 
 sudo singularity -v build ubuntu-16.04.sif singularity.ubuntu.def 
 
 sudo singularity -v build unicycler-0.4.8.sif unicycler.singularity.ubuntu.def
 ```
 
 ## Envoyer l'image vers le registry de forgemia
 
 Les `LOGIN/PASSWORD` de forgemia sont nécessaires pour envoyer l'image `unicycler-0.4.8.sif` créee précédemment avec la commande suivante
 
 ```
 singularity push --docker-username [LOGIN] --docker-password [PASSWORD] unicycler-0.4.8.sif oras:registry.forgemia.inra.fr/mandiayba/unicycler-singularity-recipe/unicycler:0.4.8
 ```